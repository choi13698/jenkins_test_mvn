package mobomobo.controller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import mobomobo.dao.face.AdminDao;
import mobomobo.dto.Movie;
import mobomobo.dto.MovieAward;
import mobomobo.dto.MovieBest;
import mobomobo.dto.MovieBestImg;
import mobomobo.dto.UserInfo;
import mobomobo.service.face.AdminService;
import mobomobo.service.face.MovieService;
import mobomobo.util.AdminMovieRecomPaging;
import mobomobo.util.MovieBestPaging;
import mobomobo.util.Paging;

@Controller
public class AdminController {
	
	//濡쒓퉭 媛앹껜
		private static final Logger logger
		= LoggerFactory.getLogger(AdminController.class);
		
		@Autowired
		private AdminService adminService;
		
		@Autowired
		private MovieService movieService;
		@Autowired ServletContext context;
		
		@Autowired
		private AdminDao adminDao;


		@RequestMapping(value="/admin/main")
		public void main() {
	
			logger.info("愿�由ъ옄�럹�씠吏�");
	
			}


		
		
		@RequestMapping(value="/admin/tables")
		public void tables() {
			
			logger.info("寃뚯떆�뙋 �뀒�씠釉� �삁�떆");
			
		}
		
		@RequestMapping(value="/admin/movierecom")
		public void movierecom(Model model, @RequestParam(defaultValue="1") int curPage) {
		
			AdminMovieRecomPaging moviepaging = adminService.getAdminMovieListPaging(curPage);
			
			List<MovieAward> list = adminService.getAwardMovieList(moviepaging);
			
			model.addAttribute("list", list);
			model.addAttribute("paging", moviepaging);
		}

		
		@RequestMapping(value = "/admin/usermanagement", method = RequestMethod.GET)
		public void usermanager(Paging userPaging, Model model) {
			
			logger.info("/usermanagement �슂泥� �셿猷� ");
				
			
//			�럹�씠吏� 怨꾩궛
			Paging paging = adminService.getPaging( userPaging );
			paging.setSearch( userPaging.getSearch() );
			logger.info("�럹�씠吏� : {}", paging);
			
			
			//�럹�씠吏뺢퀎�궛�썑 �쉶�썝愿�由� 寃뚯떆�뙋 list 留뚮뱾湲�(?)
			List<UserInfo> list = adminService.list( paging ); 
			
//			for ( int i=0; i< list.size(); i++ ) {
//				//�뀒�뒪�듃
//				logger.info( list.get(i).toString() );
//			}


			//紐⑤뜽媛믪쟾�떖
			model.addAttribute("list", list);
			model.addAttribute("paging", paging);			
			
			
		}
		
		@RequestMapping(value = "/admin/userDelete", method = RequestMethod.POST)
		public String userDelete(UserInfo userno, Model model) {
			
			logger.info("/admin/userDelete - [POST] �슂泥� �셿猷�");
			
			logger.info("�궘�젣 �릺�뼱�빞 �븯�뒗 userno : {}", userno);
			
			boolean success = adminService.userDelete(userno);
			
			logger.info("諛섑솚�릺�뼱 �룎�븘�삤�뒗媛� : {}", success);
			
			//jsp濡� 蹂대깂
			model.addAttribute("success",success);
			
			return "jsonView";
		}
		
		@RequestMapping(value = "/admin/userUpdate", method = RequestMethod.POST)
		public String userUpdate(UserInfo userInfo, Model model) {
			
			logger.info("/admin/userUpdate - [POST] �슂泥� �셿猷�");
			
			logger.info("蹂�寃� �릺�뼱�빞 �븯�뒗 userInfo : {}", userInfo);
			
			boolean success = adminService.userUpdate(userInfo);
			
			logger.info("諛섑솚�릺�뼱 �룎�븘�삤�뒗媛� : {}", success);
			
			model.addAttribute("success",success);

			return "redirect:/admin/usermanagement";
		}
		
		@RequestMapping(value="/admin/movierecomSearch")
		public @ResponseBody List<Movie> movierecomSearch(String search) throws IOException, ParseException {
			
			List<Movie> list = movieService.adminMovieSearchList(search);
			
			return list;
		}
		
		@RequestMapping(value="/admin/movierecomWrite", method=RequestMethod.POST)
		public String movierecomWrite(String division, String title, String key, String image) {
			
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("division", division);
			map.put("title", title);
			map.put("key", key);
			map.put("image", image);
			
			adminService.writeMovierecom(map);
			
			return "redirect:/admin/movierecom";
		}
		
		@RequestMapping(value="/admin/movierecomDelete")
		public @ResponseBody void movierecomDelete(MovieAward movieAward) {
			
			adminService.removeMovierecom(movieAward);
		}
		
		@RequestMapping(value="/admin/movie/adminmoviebestlist")
		public void moviebest(MovieBestPaging inData, Model model) {
			
			logger.info("愿�由ъ옄 紐낆옣硫� 寃뚯떆�뙋 �럹�씠吏�");
			
			MovieBestPaging paging = adminService.getPaging(inData);
			
			List<MovieBest> list = adminService.movieBestlist(paging);
			
			for( int i=0; i<list.size(); i++ ) {
				logger.info( list.get(i).toString() );
			}
			
			//紐⑤뜽媛� �쟾�떖
			model.addAttribute("list", list);
			model.addAttribute("paging", paging);
			
		}
		
		
		@RequestMapping(value="/admin/movie/adminmoviebestwrite", method = RequestMethod.GET)
		public void moviebestwrite() {
			
			
		}
		
		@RequestMapping(value="/admin/movie/adminmoviebestwrite", method = RequestMethod.POST)
		public String moviebestwriteProc(MovieBest movieBest, MultipartFile[] file, HttpSession session, MultipartHttpServletRequest multi) {
			
			adminDao.movieBestInsert(movieBest);
			
			
		//	logger.debug("湲��벐湲� : {}", file);
			
		//	logger.debug("湲��벐湲� : {}", movieBest);
			
		//	
		//	adminService.movieBestWrite(movieBest,file);
			
			
			String storedPath = context.getRealPath("emp");
			
			//�뤃�뜑媛� 議댁옱�븯吏� �븡�쑝硫� �깮�꽦�븯湲�
			File stored = new File(storedPath);
			if( !stored.exists() ) {
				stored.mkdir();
			}
			
			for(MultipartFile multipartFile : file) {
				logger.info("--------------------------------------");
				logger.info("Upload File Name : " + multipartFile.getOriginalFilename());
				logger.info("Upload File Size : " + multipartFile.getSize());
			
	
				String originName = multipartFile.getOriginalFilename(); //�썝蹂명뙆�씪紐�
				
				//�썝蹂명뙆�씪�씠由꾩뿉 UUID異붽��븯湲� (�뙆�씪紐낆씠 以묐났�릺吏��븡�룄濡� �꽕�젙)
				String storedName = originName + UUID.randomUUID().toString().split("-")[4];
				
				File saveFile = new File(storedPath, storedName);
				
				try {
					multipartFile.transferTo(saveFile);
				}catch (Exception e) {
					logger.error(e.getMessage());
				}
				
				
				MovieBestImg movieBestImg = new MovieBestImg();
				
				movieBestImg.setMovieBestNo(movieBest.getMovieBestNo());
				movieBestImg.setOriginName(originName);
				movieBestImg.setStoredName(storedName);
				
				adminDao.movieBestInsertFile(movieBestImg);
			}
			
			return "redirect:/admin/movie/adminmoviebestlist";
		}
		
	
		@RequestMapping(value="/admin/movie/moviebestdelete")
		public String moviebestdelete(MovieBest movieBest, MovieBestImg moiveBestImg) {
			
			
			adminService.moiveBestDelete(movieBest);

			return "redirect:/admin/movie/adminmoviebestlist";
		}
	
		
}